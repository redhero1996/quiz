@extends('main')

@section('title', '| Topics')

@section('stylesheets')
	{!! Html::style('css/styles.css') !!}
@endsection

@section('content')

	@if (Auth::check())
		@if(!empty($data))
			<div class="form-group" style="padding-left: 36px;">
				<h3 id="title-quiz" >
					<strong>Đề thi về {{$topic->name}}</strong>
				</h3>
				<h3 id="currentQuestionNumberText">Bài thi gồm {{count($data)}} câu hỏi
					<span id="fifteen_min"> (15 phút)</span>
					<button class="btn btn-warning" id="btn-test" style="float: right;">Làm bài thi</button>
					<a href="{{ route('topic', $topic->id) }}" class="btn btn-default" type="submit" id="btn-refresh" style="display: none; float:right; padding: 7px 15px;"> Làm lại </a>
				</h3>
				<hr>
			</div>

			<div class="form-group" id="score" style="margin-left: 36px;"></div>
			<div class="form-group" id="check-all">
				@php
					$i =1;
				@endphp
				@foreach ($data as $key => $value)

					<ol class="questions" style="list-style: none;">

						<li class="title-question">

							<span style="font-size: 18px; vertical-align: top;">Câu {{$i}}. </span>
							<span id="question" style="display: inline-block; margin-top: 3px;">{!! $value['question']->content !!}
							</span>
						</li>
						<ul style="list-style: none; margin-bottom: 15px;">
							@foreach ($value['answer'] as $key => $answer)
								<li class="container-fluid">
									 <label><input type="checkbox" id="{{$answer->id}}"  name="{{$answer->id}}" value="{{$answer->id}}" style="display: none;"> {{ $alphabet[$key] }}. <strong>{{ $answer->content }}</strong></label>
								</li>
							@endforeach
						</ul>
					</ol>
				@php
					$i++;
				@endphp
				@endforeach

			</div>
		@else
			không có câu hỏi
		@endif

	@else
		<p class="text-center">Vui lòng <a href="{{ route('login') }}">Đăng nhập</a> để làm bài</p>

	@endif

@stop

@section('scripts')
	<script>
		var quiz_request = {
			obj : @json($data),
			topic : {!! $topic->id !!}
		}
	</script>
	{!! Html::script('js/quiz.js') !!}
@stop

