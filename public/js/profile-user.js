	// Check password
	$(document).ready(function(){
		$('#changePassword').change(function(){
			if($(this).is(':checked')){
				$('.password').removeAttr('disabled');
			}else{
				$('.password').attr('disabled', '');
			}
		});
	});

	// Avatar
	$(function(){
	  $('#upload').change(function(){
	    var input = this;
	    var url = $(this).val();
	    var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
	    if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) 
	     {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	           $('#img').attr('src', e.target.result);
	           $('#img').css({"width" : "200px", "height" : "200px"});

	        }
	       reader.readAsDataURL(input.files[0]);
	    }
	    else
	    {
	      $('#img').attr('src', avatar.avatar);
	    }
	  });

	});