
$(document).ready(function() {

      $('ul.check > li').on('click', function(){
        $('div.btn-all').show();
        var category_id = $(this).val();
        var li_current = $(this).parents().find('li');

        if(li_current.hasClass('active')){
              li_current.removeClass('active').removeAttr('style');
              $(this).addClass('active').css('background-color', '#c2c2a3');
        }
        
        $.ajax({
           url: 'category/'+category_id,
           method: "GET",
          datatype: "json",
          success: function(data){

            $('.topics').empty();
            $.each(data, function(key, topic){
              if(topic.user){
                $('.topics').append(
                    `<a href="topics/`+topic.id+`">
                          <div class="col-sm-4 well" id="topic_id">
                            <span class="glyphicon glyphicon-ok-sign text-success" style="float: right;"></span>
                            <h4 style="height: 40px;">`+topic.name+`</h4>
                            <span id="created_at">
                              ` + moment(`${topic.created_at}`).format('DD/MM/YYYY') + `
                            </span>
                          </div>
                      </a>`
                  );  
              } else {
                $('.topics').append(
                    `<a href="topics/`+topic.id+`">
                          <div class="col-sm-4 well" id="topic_id">
                            <span class="glyphicon glyphicon-ok-sign text-success" style="display: none;"></span>
                            <h4 style="height: 40px;">`+topic.name+`</h4>
                            <span id="created_at">
                              ` + moment(`${topic.created_at}`).format('DD/MM/YYYY') + `
                            </span>
                          </div>
                      </a>`
                  );
                
              }
            });
            $('div.text-center').remove();
          }
        });
      });
    });