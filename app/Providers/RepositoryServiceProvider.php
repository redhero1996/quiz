<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \App\Repositories\Users\UserRepository::class, 
            \App\Repositories\Users\UserRepositoryEloquent::class);

        $this->app->bind(
            \App\Repositories\Categories\CategoryRepository::class, 
            \App\Repositories\Categories\CategoryRepositoryEloquent::class);
        
        $this->app->bind(
            \App\Repositories\Topics\TopicRepository::class, 
            \App\Repositories\Topics\TopicRepositoryEloquent::class);

        $this->app->bind(
            \App\Repositories\Answers\AnswerRepository::class, 
            \App\Repositories\Answers\AnswerRepositoryEloquent::class);
        
        $this->app->bind(
            \App\Repositories\Questions\QuestionRepository::class, 
            \App\Repositories\Questions\QuestionRepositoryEloquent::class);

        $this->app->bind(
            \App\Repositories\HomeRepository::class, 
            \App\Repositories\HomeRepositoryEloquent::class);
        
        $this->app->bind(
            \App\Repositories\QuizRepository::class, 
            \App\Repositories\QuizRepositoryEloquent::class);
        //:end-bindings:
    }
}
