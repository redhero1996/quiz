<?php

namespace App\Repositories\Topics;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Topics\TopicRepository;
use App\Entities\Topics\Topic;
use App\Validators\Topics\TopicValidator;

/**
 * Class TopicRepositoryEloquent.
 *
 * @package namespace App\Repositories\Topics;
 */
class TopicRepositoryEloquent extends BaseRepository implements TopicRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Topic::class;
    }

    public function slug($field, $slug){
        return $this->model->where($field, '=', $slug)->first();
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
