<?php

namespace App\Repositories\Topics;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TopicRepository.
 *
 * @package namespace App\Repositories\Topics;
 */
interface TopicRepository extends RepositoryInterface
{
    public function slug($field, $slug);
}
