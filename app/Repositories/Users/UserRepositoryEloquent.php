<?php

namespace App\Repositories\Users;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Users\UserRepository;
use App\Entities\Users\User;
use App\Validators\Users\UserValidator;
use Image;
use Storage;

/**
 * Class UserRepositoryEloquent.
 *
 * @package namespace App\Repositories\Users;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }
    
    public function handleUploadImage($boolean = false, $image, $user = null){
        if($boolean == true){
            if(!is_null($image)){
                $avatar = $image;
                $filename = time().'.'.$avatar->getClientOriginalExtension();
                $location = public_path('images/'.$filename);
                Image::make($avatar)->resize(200, 200)->save($location);
                return $this->avatar = $filename;    
            }else{
                return $this->avatar = 'avatar-two.png';
            }
        }else{
            if(!is_null($image)){
                $avatar = $image;
                $filename = time().'.'.$avatar->getClientOriginalExtension();
                $location = public_path('images/'.$filename);
                Image::make($avatar)->resize(200, 200)->save($location);
                if(isset($user)){
                    // get the old photo
                    $oldImage = $user->avatar;
                    // update the database
                    $user->avatar = $filename;
                    // delete the old photo
                    Storage::delete($oldImage);
                    return $this->avatar = $filename;
                }else{
                    return $this->avatar = $user->avatar;
                }  
            }
        }
    }
                
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
