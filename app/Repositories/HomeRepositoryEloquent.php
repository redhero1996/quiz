<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\HomeRepository;
use App\Entities\Home;
use App\Validators\HomeValidator;
use Mail;

/**
 * Class HomeRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class HomeRepositoryEloquent extends BaseRepository implements HomeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Home::class;
    }

    public function handleContact($data){
        return Mail::send('emails.contact', $data, function ($message) use ($data){
            $message->from($data['email']);
        
            $message->to('herogustin1986@gmail.com');
        
            $message->subject($data['subject']);
        
        });
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
