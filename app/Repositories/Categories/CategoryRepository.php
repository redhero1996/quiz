<?php

namespace App\Repositories\Categories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CategoryRepository.
 *
 * @package namespace App\Repositories\Categories;
 */
interface CategoryRepository extends RepositoryInterface
{
    public function slug($field, $slug);
}
