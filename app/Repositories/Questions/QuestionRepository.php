<?php

namespace App\Repositories\Questions;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface QuestionRepository.
 *
 * @package namespace App\Repositories\Questions;
 */
interface QuestionRepository extends RepositoryInterface
{
    public function getById($id);

    public function handleAnswers($id ,array $reqCorrect = [], array $reqAnswer = []);

    public function handleTopics($query);

    public function handleUpdateCorrectAns($id, array $reqCorrect = []);

    public function handleUpdateAnswers(array $reqAnswer = [], $id);
}
