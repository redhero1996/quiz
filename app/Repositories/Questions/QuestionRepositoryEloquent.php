<?php

namespace App\Repositories\Questions;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Questions\QuestionRepository;
use App\Entities\Questions\Question;
use App\Entities\Answers\Answer;
use App\Validators\Questions\QuestionValidator;

/**
 * Class QuestionRepositoryEloquent.
 *
 * @package namespace App\Repositories\Questions;
 */
class QuestionRepositoryEloquent extends BaseRepository implements QuestionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Question::class;
    }

    public function handleAnswers($id ,array $reqCorrect = [], array $reqAnswer = []){
        $question = $this->model->find($id);
        $correct = $question->correct_ans;
        if(isset($reqAnswer)){            
            foreach ($reqAnswer as $key => $content) {
                $answer = new Answer();
                $answer->content = $content;
                $answer->question()->associate($question->id);
                $question->answers()->save($answer);

                if(isset($reqCorrect)){
                    for ($i=0; $i < count($reqCorrect); $i++) { 
                        if($key == (int)$reqCorrect[$i]){
                            $correct[$i] =  $answer->id;
                            $question->correct_ans = $correct;
                        }
                        $question->save();
                    }           
                }
            }
        }
    }

    public function handleTopics($query){
        $tops = [];
        if(isset($query)){            
            foreach ($query as $topic) {
                $tops[$topic->id] = $topic->name;
            }
        }
        return $tops;
    }

    public function handleUpdateCorrectAns($id, array $reqCorrect = []){
        $question = $this->model->find($id); 
        // Reset all correct answers
        $question->correct_ans = null;
        $correct = $question->correct_ans;
        // update all correct answers
        if(isset($reqCorrect)){
            for ($i=0; $i < count($reqCorrect); $i++) {   
                $correct[$i] = (int)$reqCorrect[$i];          
                $question->correct_ans = $correct;
            }
            return $question->correct_ans;
        }
    }

    public function handleUpdateAnswers(array $reqAnswer = [], $id){
        $answers = Answer::where('question_id', $id)->with('question')->get();
        if(isset($reqAnswer)){
            for($i=0; $i < count($reqAnswer); $i++){
                $answer = Answer::find($answers[$i]->id);
                if($reqAnswer[$i] != $answers[$i]->content){
                    $answer->content = $reqAnswer[$i];
                    $answer->save();
                }   
            }
        }
    }
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
