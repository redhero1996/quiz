<?php

namespace App\Repositories\Answers;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Answers\AnswerRepository;
use App\Entities\Answers\Answer;
use App\Validators\Answers\AnswerValidator;

/**
 * Class AnswerRepositoryEloquent.
 *
 * @package namespace App\Repositories\Answers;
 */
class AnswerRepositoryEloquent extends BaseRepository implements AnswerRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Answer::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
