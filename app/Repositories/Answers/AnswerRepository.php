<?php

namespace App\Repositories\Answers;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AnswerRepository.
 *
 * @package namespace App\Repositories\Answers;
 */
interface AnswerRepository extends RepositoryInterface
{
    //
}
