<?php

namespace App\Entities\Questions;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use App\Entities\Topics\Topic;
use App\Entities\Answers\Answer;
/**
 * Class Question.
 *
 * @package namespace App\Entities\Questions;
 */
class Question extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['content', 'correct_ans'];
    protected $table = 'questions';
	protected $casts = [
		'correct_ans' => 'array',
	];

    public function topics(){
    	return $this->belongsToMany(Topic::class);
    }

    public function answers(){
    	// return $this->hasMany('App\Question', 'answers', 'question_id');
    	return $this->hasMany(Answer::class);
    }
}
