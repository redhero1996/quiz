<?php

namespace App\Entities\Topics;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use App\Entities\Categories\Category;
use App\Entities\Questions\Question;
use App\Entities\Users\User;
/**
 * Class Topic.
 *
 * @package namespace App\Entities\Topics;
 */
class Topic extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug', 'category_id'];
    protected $table = 'topics';

    	public function category(){
    		return $this->belongsTo(Category::class);
    }
    public function questions(){
    		return $this->belongsToMany(Question::class);
    }
    public function users(){
    		return $this->belongsToMany(User::class)->withPivot('total');
    }
}
