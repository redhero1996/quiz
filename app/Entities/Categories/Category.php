<?php

namespace App\Entities\Categories;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use App\Entities\Topics\Topic;
/**
 * Class Category.
 *
 * @package namespace App\Entities\Categories;
 */
class Category extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'categories';
    protected $fillable = ['name', 'slug'];

	public function topics(){
	   	return $this->hasMany(Topic::class);
	}

}
