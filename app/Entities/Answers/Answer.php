<?php

namespace App\Entities\Answers;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use App\Entities\Questions\Question;
/**
 * Class Answer.
 *
 * @package namespace App\Entities\Answers;
 */
class Answer extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['content', 'question_id'];
    protected $table = 'answers';
    // protected $primaryKey = 'question_id';

    public function question(){
    	return $this->belongsTo(Question::class);
    }
}
