<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'subject' => 'min:3',
            'message' => 'min:10',
        ];
    }

    public function messages(){
        return [
            'email.required' => 'Vui lòng nhập email',
            'subject.min' => 'Tiêu đề tối tiểu 3 ký tự',
            'message.min' => 'Nội dung tối thiểu 10 ký tự' 
        ];
    }
}
