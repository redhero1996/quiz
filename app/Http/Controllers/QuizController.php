<?php

namespace App\Http\Controllers;

use App\Entities\Categories\Category;
use App\Entities\Questions\Question;
use App\Entities\Topics\Topic;
use App\Entities\Users\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QuizController extends Controller
{
    public function category($id)
    {
        $topics = Topic::where('category_id', $id)->latest('id')->get(['id', 'name', 'created_at']);
        foreach ($topics as $key => $topic) {
            foreach ($topic->users as $user) {
                if (Auth::check() && (Auth::user()->id == $user->pivot->user_id)) {
                    $topics[$key]['user'] = $user->pivot->user_id;
                }
            }
        }
        return response()->json($topics);
    }

    public function topic($id)
    {
        $alphabet = array('A', 'B', 'C', 'D', 'E',
            'F', 'g', 'h', 'i', 'j',
            'k', 'l', 'm', 'n', 'o',
            'p', 'q', 'r', 's', 't',
            'u', 'v', 'w', 'x', 'y',
            'z');
        $topic = Topic::find($id);
        $questions = $topic->questions()->where('topic_id', $id)->get();
        $data = [];
        foreach ($questions as $question) {
            $answers = Question::find($question->id)->answers()->get();
            $data[] = [
                'question' => $question,
                'answer' => $answers,
            ];
        }
        return view('pages.topic', ['data' => $data,
            'alphabet' => $alphabet,
            'topic' => $topic,
        ]);
    }

    function correct(Request $request)
    {
        $correctAns = [];
        $dataRequest = $request->dataRequest;
        $score = 0;
        $total = 0;
        $topic_id = $dataRequest[0]['topic'];
        // dd($dataRequest);
        foreach ($dataRequest as $key => $value) {
            $question = Question::find($value['question_id']);
            if (isset($value['answer'])) {
                $value['answer'] = array_map(function ($elem) {
                    return intval($elem);
                }, $value['answer']);
                if ((count($value['answer']) == count($question->correct_ans)) && !array_diff($value['answer'], $question->correct_ans)) {
                    $score++;
                    $total += 5;
                    $correctAns[] = [
                        'question_id' => $value['question_id'],
                        'answer_id' => $value['answer'],
                        'answer' => true,
                    ];
                } else {
                    $correctAns[] = [
                        'question_id' => $value['question_id'],
                        'answer_id' => isset($value['answer']) ? $value['answer'] : -1,
                        'answer' => false,
                        'correct_ans' => $question->correct_ans,
                    ];
                }
            } else {
                $correctAns[] = [
                    'question_id' => $value['question_id'],
                    'answer_id' => -1,
                    'answer' => false,
                    'correct_ans' => $question->correct_ans,
                ];
            }
        }

        // Save total
        Auth::user()->topics()->attach($topic_id, ['total' => $total]);

        $dataReponse = [
            'score' => $score,
            'total' => $total,
            'correctAns' => $correctAns,
        ];

        return $dataReponse;
    }
}
