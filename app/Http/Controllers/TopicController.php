<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use App\Http\Requests\TopicRequest;
use Session;
use App\Repositories\Topics\TopicRepository;
use App\Entities\Topics\Topic;
use App\Entities\Categories\Category;
use App\Entities\Questions\Question;

class TopicController extends Controller
{
    protected $topicRepository;
    public function __construct(TopicRepository $topicRepository){
        $this->topicRepository = $topicRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topics = $this->topicRepository->all();
        return view('admin.topics.index')->withTopics($topics);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.topics.create', [ 'categories' => $categories ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TopicRequest $request)
    {
        $data = $request->all();
        $data['slug'] = str_slug($request->name, '-');
        
        $topic = $this->topicRepository->create($data);

        Session::flash('success', 'The topic was successfully saved!');
        return redirect()->route('topics.show', $topic->slug);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $topic = $this->topicRepository->slug('slug', $slug);
        return view('admin.topics.show')->withTopic($topic);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $topic = $this->topicRepository->find($id);
        $categories = Category::all();

        $cats = [];
        foreach ($categories as $category) {
            $cats[$category->id] = $category->name;
            
        }
        return view('admin.topics.edit', [ 'topic' => $topic, 'categories' => $cats ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TopicRequest $request, $id)
    {
        $data = $request->all();
        $data['slug'] = str_slug($request->name, '-');

        $topic = $this->topicRepository->update($data, $id);
        
        Session::flash('success', 'The topic was successfully updated!');
        return redirect()->route('topics.show', $topic->slug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $topic = $this->topicRepository->find($id);
        $topic->questions()->detach();
        $topic->users()->detach();
        $this->topicRepository->delete($id);

        Session::flash('success', 'The topic was successfully deleted!');
        return redirect()->route('topics.index');
    }
}
