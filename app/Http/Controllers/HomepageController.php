<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;
use Illuminate\Support\Facades\Auth;
use Session;
use App\Repositories\HomeRepository;
use App\Repositories\Users\UserRepository;
use App\Entities\Categories\Category;
use App\Entities\Topics\Topic;
use App\Entities\Users\User;

class HomepageController extends Controller
{
    protected $repository, $userRepository;
    public function __construct(HomeRepository $repository, UserRepository $userRepository){
        $this->repository = $repository;
        $this->userRepository = $userRepository;
    }

    public function index(){
    	$categories = Category::all();
    	$topics = Topic::latest('id')->paginate(12);

    	return view('pages.welcome', ['categories' => $categories, 'topics' => $topics]);
    }

    public function about(){
    	return view('pages.about');
    }

    public function getcontact(){
    	return view('pages.contact');
    }
    public function postcontact(ContactRequest $request){
        $data = [
            'email' => $request->email,
            'subject' => $request->subject,
            'bodyMessage' => $request->message,
        ];
        $this->repository->handleContact($data);
        Session::flash('success', 'Email của bạn đã được gửi!');
        return redirect('/');
    }

    // ============= User Profile ===============//
    public function getProfile($id){
        $user = $this->userRepository->find($id);
        return view('pages.profile', ['user' => $user]);
    } 

    public function postProfile(Request $request, $id){
        $user_id = $this->userRepository->find($id);
        $data = $request->validate([
            'name' => 'required',
        ],[
            'name.required' => 'Vui lòng nhập tên',
        ]);
        if($request->changePassword == 'on'){
            $request->validate([
                'password' => 'required|min:6',
                'confirm_pass' => 'required|same:password',
            ], [
                'password.required' => 'Vui lòng nhập mật khẩu',
                'password.min' => 'Mật khẩu tối thiểu 6 ký tự',
                'confirm_pass.required' => 'Mật khẩu không khớp',
                'confirm_pass.same' => 'Mật khẩu không khớp',
            ]);
            $data['password'] = bcrypt($request->password);
        }
        $data['avatar'] = $this->userRepository->handleUploadImage(false, $request->file('avatar'), $user_id);
        $user = $this->userRepository->update($data, $id);

        Session::flash('success', 'Tài khoản đã được cập nhật');
        return redirect()->route('user.profile', $user->id);
    }

    public function logout(){
        Auth::logout();
        Session::flush();
        return redirect('/');
    }
}
