<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Topics\Topic;
use App\Entities\Categories\Category;

class AjaxSelectController extends Controller
{

    public function selectAjax($category_id){
		$topics = Topic::where('category_id', $category_id)->get(['id', 'name']);
		return response()->json($topics);
    }
}
