<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Session;
use App\Entities\Users\User;
use App\Repositories\Users\UserRepository;

class UserController extends Controller
{
    protected $userRepository;
    public function __construct(UserRepository $userRepository){
        $this->userRepository = $userRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->userRepository->all();
        return view('admin.users.index')->withUsers($users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $data = $request->all();
        $data['password'] = bcrypt($request->password);
        $data['avatar'] = $this->userRepository->handleUploadImage(true, $request->file('avatar')); 

        $user = $this->userRepository->create($data);

        Session::flash('success', 'The user was successfully save!');
        return redirect()->route('users.show', $user->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->userRepository->find($id);
        return view('admin.users.show')->withUser($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->find($id);
        return view('admin.users.edit')->withUser($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_id = $this->userRepository->find($id);
        $data = $request->validate([
            'name' => 'required|min:3',
        ]);
        if($request->changePassword == 'on'){
            $request->validate([
                'password' => 'required|confirmed|min:6',
                'password_confirmation' => 'required|min:6',
            ]);
            $data['password'] = bcrypt($request->password);
        }
        $data['avatar'] = $this->userRepository->handleUploadImage(false,$request->file('avatar'), $user_id);
        $user = $this->userRepository->update($data, $id);       

        Session::flash('success', 'The user was successfully updated!');
        return redirect()->route('users.show', $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->userRepository->find($id);
        $user->topics()->detach();
        $this->userRepository->delete($id);

        Session::flash('success', 'The user was successfully deleted!');
        return redirect()->route('users.index');
    }
}
