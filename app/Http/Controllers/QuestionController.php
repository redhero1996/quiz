<?php

namespace App\Http\Controllers;

use App\Http\Requests\QuestionRequest;
use Session;
use Purifier;
use App\Repositories\Questions\QuestionRepository;
use App\Entities\Questions\Question;
use App\Entities\Categories\Category;
use App\Entities\Topics\Topic;
use App\Entities\Answers\Answer;

class QuestionController extends Controller
{
    protected $questionRepository;
    public function __construct(QuestionRepository $questionRepository){
        $this->questionRepository = $questionRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = $this->questionRepository->all();
        return view('admin.questions.index', [ 'questions' => $questions ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $topics = Topic::all();
        $categories = Category::all();
        return view('admin.questions.create', ['topics' => $topics, 'categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(QuestionRequest $request)
    {
        $data = $request->all();
        $data['content'] = Purifier::clean($request->content);
        $question = $this->questionRepository->create($data);     
 
        $this->questionRepository->syncWithoutDetaching($question->id, 'topics', $request->topic_id);

        $this->questionRepository->handleAnswers($question->id, $request->correct_ans, $request->answer);

        Session::flash('success', 'The question was successfully saved!');
        return redirect()->route('questions.show', $question->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $question = $this->questionRepository->with('topics')->getById($id);
        $answers = Answer::where('question_id', $id)->with('question')->get();

        return view('admin.questions.show', [   'question'  => $question,
                                                'answers'   => $answers,
                                                'alphabet'  => $this->questionRepository->alphabet()
                                            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $answers = Answer::where('question_id', $id)->with('question')->get();              
        $topics = $this->questionRepository->handleTopics(Topic::all());
        
        return view('admin.questions.edit', [   'question' => $this->questionRepository->find($id), 
                                                'topics' => $topics, 
                                                'answers' => $answers,
                                                'alphabet' => $this->questionRepository->alphabet() 
                                            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(QuestionRequest $request, $id)
    {
        $data = $request->all();
        $data['content'] = Purifier::clean($request->content);
        $data['correct_ans'] = $this->questionRepository->handleUpdateCorrectAns($id, $request->correct_ans);
        $question = $this->questionRepository->update($data, $id);

        $this->questionRepository->sync($question->id, 'topics', $request->topic_id, true);
        // Update all answers was be change
        $this->questionRepository->handleUpdateAnswers($request->answer, $question->id);
        
        Session::flash('success', 'The question was successfully updated!');
        return redirect()->route('questions.show', $question->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::find($id);
        $question->topics()->detach();
        $answers = Answer::where('question_id', $id)->delete();
        $question->delete();

        Session::flash('success', 'The question was successfully deleted!');
        return redirect()->route('questions.index');

    }
}
